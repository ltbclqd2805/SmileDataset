import tensorflow as tf
import numpy as np

SAVE_FOLDER = '../data/MultiTask/smile_data/'

smile_train = np.load(SAVE_FOLDER + 'train.npy')
smile_test = np.load(SAVE_FOLDER + 'test.npy')


def one_hot(index, num_classes):
    assert index < num_classes and index >= 0
    tmp = np.zeros(num_classes, dtype=np.float32)
    tmp[index] = 1.0
    return tmp


def _input():
    x = tf.placeholder(dtype=tf.float32, shape=[None, 48, 48, 1], name='input')
    y_ = tf.placeholder(dtype=tf.float32, shape=[None, 2], name='label')
    return x, y_


def _conv2d(x, out_filters, kernel_size, stride, padding='SAME'):
    in_filters = x.get_shape()[-1]

    kernel = tf.get_variable(name='kernel', dtype=tf.float32, initializer=tf.truncated_normal_initializer(stddev=0.001),
                             shape=[kernel_size, kernel_size, in_filters, out_filters])
    bias = tf.constant(0, dtype=tf.float32, shape=[out_filters], name='bias')

    h = tf.nn.conv2d(input=x, filter=kernel, strides=[1, stride, stride, 1], padding=padding, name='conv')

    output = tf.nn.relu(h + bias, name='relu')

    return output


def _flattten(x):
    shape = x.get_shape().as_list()
    new_shape = np.prod(shape[1:])
    x = tf.reshape(x, [-1, new_shape], name='flatten')
    return x


def _fc(x, out_dim, activation='linear'):
    assert activation == 'linear' or activation == 'relu'
    W = tf.get_variable('W', [x.get_shape()[1], out_dim],
                        initializer=tf.truncated_normal_initializer(stddev=0.001))
    b = tf.get_variable('bias', [out_dim], initializer=tf.constant_initializer(0))
    x = tf.nn.xw_plus_b(x, W, b, name='linear')

    if activation == 'relu':
        x = tf.nn.relu(x, name='relu')
    return x


def inference(x):
    with tf.variable_scope('Block1'):
        output = _conv2d(x, 16, 5, 1)
        output = tf.nn.max_pool(output, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pooling')

    with tf.variable_scope('Block2'):
        output = _conv2d(output, 16, 7, 1)
        output = tf.nn.max_pool(output, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pooling')

    with tf.variable_scope('Block3'):
        output = _conv2d(output, 32, 9, 1)
        output = tf.nn.max_pool(output, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pooling')

    with tf.variable_scope('FC'):
        output = _flattten(output)
        output = _fc(output, 128, 'relu')

    with tf.variable_scope('linear'):
        output = _fc(output, 2)

    return output


def _losses(logits, labels):
    l2_loss = 1e-4 * tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=labels))
    total_loss = tf.add(l2_loss, cross_entropy, name='loss')
    return total_loss


def _train_op(loss, global_step):
    learning_rate = tf.placeholder(dtype=tf.float32, name='learning_rate')
    train_step = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss, global_step)
    return learning_rate, train_step


if __name__ == "__main__":
    sess = tf.InteractiveSession()
    global_step = tf.contrib.framework.get_or_create_global_step()
    x, y_ = _input()
    logits = inference(x)
    loss = _losses(logits, y_)
    learning_rate, train_step = _train_op(loss, global_step)
    prediction = tf.nn.softmax(logits)
    correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    writer = tf.summary.FileWriter('./summary/')
    writer.add_graph(sess.graph)
    tf.summary.scalar('loss', loss)
    tf.summary.scalar('acc', accuracy)
    merge_summary = tf.summary.merge_all()
    sess.run(tf.global_variables_initializer())

    for epoch in range(100):
        np.random.shuffle(smile_train)
        train_img = []
        train_label = []
        for i in range(len(smile_train)):
            train_img.append(smile_train[i][0])
            train_label.append(one_hot(smile_train[i][1], 2))
        print('Epoch %d' % epoch)
        mean_loss = []
        mean_acc = []
        batch_size = 128
        num_batch = int(len(smile_train) // batch_size)
        for batch in range(num_batch):
            print('Training on batch .............. %d / %d' % (batch, num_batch), end='\r')
            top = batch * batch_size
            bot = min((batch + 1) * batch_size, len(smile_train))
            batch_img = np.asarray(train_img[top:bot])
            batch_label = np.asarray(train_label[top:bot])

            ttl, _, acc, s = sess.run([loss, train_step, accuracy, merge_summary],
                                      feed_dict={x: batch_img, y_: batch_label, learning_rate: 0.0005})
            writer.add_summary(s, int(global_step.eval()))
            mean_loss.append(ttl)
            mean_acc.append(acc)

        mean_loss = np.mean(mean_loss)
        mean_acc = np.mean(mean_acc)
        print('\nTraining loss: %f' % mean_loss)
        print('Training accuracy: %f' % mean_acc)

        test_img = []
        test_label = []
        for i in range(len(smile_test)):
            test_img.append(smile_test[i][0])
            test_label.append(one_hot(smile_test[i][1], 2))
        mean_loss = []
        mean_acc = []
        batch_size = 100
        num_batch = int(len(smile_test) // batch_size)
        for batch in range(num_batch):
            top = batch * batch_size
            bot = min((batch + 1) * batch_size, len(smile_test))
            batch_img = np.asarray(test_img[top:bot])
            batch_label = np.asarray(test_label[top:bot])

            ttl, acc = sess.run([loss, accuracy], feed_dict={x: batch_img, y_: batch_label})
            mean_loss.append(ttl)
            mean_acc.append(acc)

        mean_loss = np.mean(mean_loss)
        mean_acc = np.mean(mean_acc)
        print('\nTesting loss: %f' % mean_loss)
        print('Testing accuracy: %f' % mean_acc)
